import Vue from 'vue'
import Router from 'vue-router'
import PlanADrive from '@/components/How-it-Works'
import LandingPage from '@/components/LandingPage'
import LearnMore from '@/components/Learn-More'
import Gallery from '@/components/Gallery'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage
    },
    {
      path: '/plan-a-drive',
      name: 'PlanADrive',
      component: PlanADrive
    },
    {
      path: '/learn-more',
      name: 'LearnMore',
      component: LearnMore
    },
    {
      path: '/gallery',
      name: 'Gallery',
      component: Gallery
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return ({x: 0, y: 0});
  }
})
